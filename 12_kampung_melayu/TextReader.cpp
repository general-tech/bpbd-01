#include "TextReader.h"

TextReader::TextReader() {

	if (tess.Init(NULL, "eng", (tesseract::OcrEngineMode) 0)) {
		std::cout << "OCRTesseract: Could not initialize tesseract." << std::endl;
		throw 1;
	}

	tesseract::PageSegMode pagesegmode = (tesseract::PageSegMode) 7; // single text line
	tess.SetPageSegMode(pagesegmode); 
	tess.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	tess.SetVariable("save_best_choices", "T");


}

TextReader::~TextReader() {
	tess.End();
}

std::string TextReader::read(cv::Mat image) {

  cv::Size s = image.size();
	tess.SetImage((uchar*)image.data, s.width, s.height, image.channels(), image.step1());
	tess.Recognize(0);

	char *outText;
	outText = tess.GetUTF8Text();
  std::string result(outText);
	tess.Clear();

  return result; 
}


void TextReader::readDetail(cv::Mat image, std::vector<cv::Rect> *locations, std::vector<std::string> *result) {

  cv::Size s = image.size();
	tess.SetImage((uchar*)image.data, s.width, s.height, image.channels(), image.step1());
	tess.Recognize(0);

	tesseract::ResultIterator* ri = tess.GetIterator();
	tesseract::PageIteratorLevel level = tesseract::RIL_WORD;

	if (ri != 0) {
		do {
			const char* word = ri->GetUTF8Text(level);
			if (word == NULL)
				continue;
			float conf = ri->Confidence(level);
      int x1, y1, x2, y2;
      ri->BoundingBox(level, &x1, &y1, &x2, &y2);
      cv::Rect box(x1,y1, x2-x1, y2-y1);
      locations->push_back(box);
      result->push_back(word);
			delete[] word;
		} while (ri->Next(level));
	}
	delete ri;
	tess.Clear();
}

