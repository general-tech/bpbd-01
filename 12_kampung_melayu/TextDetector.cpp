#include "TextDetector.h"
#include <stdio.h>

void er_draw(
    std::vector<cv::Mat> &channels, 
    std::vector<std::vector<cv::text::ERStat> > &regions, 
    std::vector<cv::Vec2i> group, cv::Mat& segmentation)
{
  for (int r=0; r<(int)group.size(); r++)
  {
    cv::text::ERStat er = regions[group[r][0]][group[r][1]];
    if (er.parent != NULL) // deprecate the root region
    {
      int newMaskVal = 255;
      int flags = 4 + (newMaskVal << 8) + cv::FLOODFILL_FIXED_RANGE + cv::FLOODFILL_MASK_ONLY;
      floodFill(
          channels[group[r][0]],
          segmentation,
          cv::Point(er.pixel%channels[group[r][0]].cols,er.pixel/channels[group[r][0]].cols),
          cv::Scalar(255),
          0,
          cv::Scalar(er.level),
          cv::Scalar(0),
          flags
          );
    }
  }
}


TextDetector::TextDetector() {
  er_filter1 = cv::text::createERFilterNM1(cv::text::loadClassifierNM1("trained_classifierNM1.xml"),8,0.00015f,0.13f,0.2f,true,0.1f);
  er_filter2 = cv::text::createERFilterNM2(cv::text::loadClassifierNM2("trained_classifierNM2.xml"),0.5);
}

TextDetector::~TextDetector() {
  er_filter1.release();
  er_filter2.release();
}


void TextDetector::detect(cv::Mat image, std::vector<cv::Rect> *locations, std::vector<cv::Mat> *images) {

  cv::Mat grey;
  cv::cvtColor(image, grey, cv::COLOR_RGB2GRAY);
  std::vector<cv::Mat> channels;

  channels.push_back(grey);
  channels.push_back(255-grey);

  std::vector<std::vector<cv::text::ERStat> > regions(channels.size());

  // Apply the default cascade classifier to each independent channel (could be done in parallel)
  for (int c=0; c<(int)channels.size(); c++)
  {
    er_filter1->run(channels[c], regions[c]);
    er_filter2->run(channels[c], regions[c]);
  }

  cv::Mat out_img_decomposition= cv::Mat::zeros(image.rows+2, image.cols+2, CV_8UC1);
  std::vector<cv::Vec2i> tmp_group;
  for (int i=0; i<(int)regions.size(); i++)
  {
    for (int j=0; j<(int)regions[i].size();j++)
    {
      tmp_group.push_back(cv::Vec2i(i,j));
    }
    cv::Mat tmp= cv::Mat::zeros(image.rows+2, image.cols+2, CV_8UC1);
    er_draw(channels, regions, tmp_group, tmp);
    if (i > 0)
      tmp = tmp / 2;
    out_img_decomposition = out_img_decomposition | tmp;
    tmp_group.clear();
  }

  std::vector< std::vector<cv::Vec2i> > nm_region_groups;
  std::vector<cv::Rect> nm_boxes;
  cv::text::erGrouping(image, channels, regions, nm_region_groups, nm_boxes, cv::text::ERGROUPING_ORIENTATION_HORIZ);


  char name[255];
  for (int j=0; j<(int) nm_boxes.size(); j++) {
    cv::Rect box = nm_boxes[j];
    locations->push_back(box);
    //cv::rectangle(out_img_decomposition, box.tl(), box.br(), cv::Scalar(255),3);
    cv::Mat single(out_img_decomposition, box);
    cv::Mat singleSmall;
    single.copyTo(singleSmall);
    images->push_back(singleSmall);
  }
}
