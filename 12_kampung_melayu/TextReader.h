#include <tesseract/baseapi.h>
#include <opencv2/opencv.hpp>

class TextReader {
	private:
		tesseract::TessBaseAPI tess;

	public:
		TextReader();
		~TextReader();
    std::string read(cv::Mat image);
    void readDetail(cv::Mat image, std::vector<cv::Rect> *locations, std::vector<std::string> *result);
};
