#include <opencv2/opencv.hpp>
#include <opencv2/text.hpp>

class TextDetector {

  private:
    cv::Ptr<cv::text::ERFilter> er_filter1;
    cv::Ptr<cv::text::ERFilter> er_filter2;

  public:
    TextDetector();
    ~TextDetector();
    void detect(cv::Mat image, std::vector<cv::Rect> *locations, std::vector<cv::Mat> *images);
};
