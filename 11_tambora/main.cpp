#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
#include "TextDetector.h"
#include "TextReader.h"
#include "stdio.h"
#include <sstream>
#include "db.h"

TextDetector detector;

TextReader reader;

cv::Mat img;
cv::Mat imgc;


DB db;

int frameCount = 0;

void detect() {

  img.copyTo(imgc);

  std::vector<cv::Rect> locations;
  std::vector<cv::Mat> images;

  detector.detect(imgc, &locations, &images);

  std::stringstream result;

  for (int j=0; j<images.size(); j++) {
    result << reader.read(images[j]);
  }

  std::string needle1("KEBAKARAN");
  std::string needle2("AMBULANCE");
  std::string resultstr = result.str();

  char filename[128];
  if (resultstr.find(needle1) != std::string::npos) {
    sprintf(filename, "img/kebakaran-%d.jpg", frameCount);
    cv::imwrite(filename, imgc);
    db.insert(needle1, filename);
  } else if (resultstr.find(needle2) != std::string::npos) {
    sprintf(filename, "img/ambulance-%d.jpg", frameCount);
    cv::imwrite(filename, imgc);
    db.insert(needle2, filename);
  }

  frameCount++;
}


int main(int argc, char *argv[]) {
  db.init("192.168.100.50", "root", "bersaudara", "bpbd_jkt");


  cv::VideoCapture cap(argv[1]);
  cv::Mat img_input;
  cv::Mat mask = cv::imread(argv[2]);

  int frameCountSync = frameCount;
  boost::thread *t;

  while(cv::waitKey(1) != 27) {

    if (!cap.isOpened()) {
      cap.open(argv[1]);
    }
    
    cap >> img_input;

    img_input.copyTo(img, mask);


    if (frameCountSync == frameCount) {
      frameCountSync++;
      if (frameCount > 0)  {
        t->join();
        delete t;
      }
      t = new boost::thread(detect);
    }

    // cv::imshow("w", img_input);
    // std::cout.write((char *) img_input.data, img_input.total() * img_input.elemSize());
  }

  //cv::imshow("w", img);
  //cv::waitKey(10000);
}

