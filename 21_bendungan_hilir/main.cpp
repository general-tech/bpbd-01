#include <iostream>
#include <opencv2/opencv.hpp>
#include <chrono>
#include <stdio.h>

#include "package_bgs/PBAS/PixelBasedAdaptiveSegmenter.h"
#include "package_tracking/BlobTracking.h"
#include "package_analysis/VehicleCouting.h"
#include "db.h"

int frameCount;

int main(int argc, char *argv[])
{

  if (argc < 4) {
    std::cout << "./a.out video mask.jpg s" << std::endl;
    std::cout << "./a.out video mask.jpg l" << std::endl;
    return -1;
  }

  frameCount = 0;
  bool stream = false;
  if (*argv[3] == 's') {
    stream = true;
  }

  DB db;
  db.init("192.168.100.50", "root", "bersaudara", "bpbd_jkt", "report", "total", argv[1]);
  auto last_time_insert = std::chrono::system_clock::now();


  /* Open video file */
  cv::Mat img_input;
  cv::Mat img;
  cv::VideoCapture cap(argv[1]);
  cv::Mat mask = cv::imread(argv[2]);

  /* Background Subtraction Algorithm */
  PixelBasedAdaptiveSegmenter *bgs;
  bgs = new PixelBasedAdaptiveSegmenter;
  
  /* Blob Tracking Algorithm */
  cv::Mat img_blob;
  BlobTracking* blobTracking;
  blobTracking = new BlobTracking;

  /* Vehicle Counting Algorithm */
  VehicleCouting* vehicleCouting;
  vehicleCouting = new VehicleCouting;

  while(cv::waitKey(1) != 27)
  {
    frameCount++;
    cap >> img_input;

    if (!cap.isOpened()) {
      cap.open(argv[1]);
    }


    try {
      cv::resize(img_input, img_input, cv::Size(960, 540));
    } catch(cv::Exception e) {
      break;
    }

    img_input.copyTo(img, mask);
  
    // bgs->process(...) internally process and show the foreground mask image
    cv::Mat img_mask;
    bgs->process(img, img_mask);
    
    if(!img_mask.empty())
    {
      // Perform blob tracking
      blobTracking->process(img_input, img_mask, img_blob);

      // Perform vehicle counting
      vehicleCouting->setInput(img_blob);
      vehicleCouting->setTracks(blobTracking->getTracks());
      vehicleCouting->process();

      auto now = std::chrono::system_clock::now();
      int duration = std::chrono::duration_cast<std::chrono::seconds>(now - last_time_insert).count();
      if (duration > 300) {
        char filename[128];
        sprintf(filename, "img/kendaraan-%d.jpg", frameCount);
        cv::imwrite(filename, img_input);
        if (vehicleCouting->showAB == 1) {
          db.insert((float) vehicleCouting->countAB, std::string("MOBIL"), std::string(filename));
          db.insert((float) vehicleCouting->countMAB, std::string("MOTOR"), std::string(filename));
        } else if (vehicleCouting->showAB == 2) {
          db.insert((float) vehicleCouting->countBA, std::string("MOBIL"), std::string(filename));
          db.insert((float) vehicleCouting->countMBA, std::string("MOTOR"), std::string(filename));
        }
        vehicleCouting->countAB = 0;
        vehicleCouting->countBA = 0;
        vehicleCouting->countMAB = 0;
        vehicleCouting->countMBA = 0;
        last_time_insert = now;
      }

      if (stream) {
        cv::Mat img = vehicleCouting->img_input;
        std::cout.write((char *) img.data, img.total() * img.elemSize());
      }
    }
  }

  delete vehicleCouting;
  delete blobTracking;
  delete bgs;

  cvDestroyAllWindows();
  cap.release();
  
  return 0;
}
